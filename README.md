# smallcms
Smallcms is a simple content management system that runs as a CGI script, exposing the ability for simple page edits from the web.

Smallcms prefers to reside in `/admin/` and acts on `/index.html`. Smallcms will enumerate any nodes with a class id that ends in `-editable` and present them as a simple text box that can be updated. Smallcms converts between `<br>` and `\n` as appropriate to ease editing on mobile devices. The content of the text box, including any HTML will be directly inserted into the page.

## Dependencies
* [Mojo::DOM](https://metacpan.org/pod/Mojo::DOM)
* [File::Slurp](https://metacpan.org/pod/File::Slurp)
* [CGI](https://metacpan.org/pod/distribution/CGI/lib/CGI.pod)

`App::cpanminus` and `local::lib` may be of interest to those maintaining a local perl installation.

## Installation
Smallcms is a perl CGI program. Configuration will be specific to your server. Smallcms should be installed under the `/admin/` directory in your server root, and will edit `/index.html` in the server root.

### NGINX and fcgiwrap
```
nginx.conf
location ^~ /admin {
        auth_basic "Restricted";
        auth_basic_user_file /home/pirateninjas.xyz/public_html/admin/.htpasswd;
        include        /opt/local/etc/nginx/fastcgi_params;
        fastcgi_pass   unix:/var/run/fastcgi.sock;
}

# Redundant?
location ~  \.pl$ {
        include        /opt/local/etc/nginx/fastcgi_params;
        fastcgi_pass   unix:/var/run/fastcgi.sock;
}
```

And run fcgiwrap with `fcgiwrap -s unix:/var/run/fastcgi.sock`.
