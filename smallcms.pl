#!/usr/bin/perl
use warnings;
use strict;

use Mojo::DOM;
use File::Slurp;
use CGI;

my $filename = '../index.html';
my $text = read_file($filename);
my $cgi = CGI->new;
my $dom = Mojo::DOM->new($text);

my $selector = "-editable";
my @fields = $dom->find("[id\$=$selector]")->attr('id')->each;

my $formbuilder = "";
foreach my $field (@fields) {
	$dom->at("#$field")->content(grep(s/\n/<br \/>/g, $cgi->param("$field"))) if ($cgi->param("$field"));
	my $contents = $dom->at("#$field")->content;
	$contents =~ s/<br.*?>/\n/g;
	$formbuilder .= "<b>$field:</b><br />\n";
	$formbuilder .= "<textarea name=\"$field\" cols=\"60\" rows=\"7\">$contents</textarea><br /><br />\n";
}

print $cgi->header('text/html');
print << "EOF";
<html>
<head><title>Update news</title></head>
<body>
<h1>Update news</h1>
<form method="post">
$formbuilder
<input type="submit" name="save" value="save" />
</body>
</html>
EOF

write_file($filename, $dom);
